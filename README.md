Exercício de Programação 1

Programa passado pelo professor Renato Coral pela disciplina Orientação a Objeto
que observar a compreensão prática do aluno no decorrer da matéria, esse primeiro
Exercício de Programação busca avaliar os seguintes conceito: 
"
-Criação de classes e objetos.
-Utilização de atributos, metódos e contrutores.
-Utilização de herança."

"O EP1 tem como objetivo principal a implementação de um programa que seja capaz
de ler arquivos de imagens nos formatos PGM e PPM e extrair informações nestas 
imagens atrávez da técnica da esteganografia."

Como compilar o EP1:
-Abrir o terminal.
-Ir ate o local do Programa.
-comando: make.
-comando: make run.

Autor: 
Vinícius Rodrigues Oliveira
14/0165291
