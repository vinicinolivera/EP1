#include "imagem.hpp"
#include <fstream>
#include "decifraPPM.hpp"
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <iomanip>
#include <cstring>
#include <cstdlib>
#include <bitset>
#include <istream>
#define getbit(by, pos) ((by >>pos)&0x1)

using namespace std;

Imagem::Imagem(){
  setTipoImagem("sem valor");
  setAltura(0);
  setLargura(0);
  setComentario("sem valor");
  setNumero_Magico("sem valor");
  setNumero_Maximo_Cor(0);
}

// Imagem::Imagem(){
//
// }

void Imagem::setTipoImagem(string imagem){
  this->imagem = imagem;
}
void Imagem::setAltura(float altura){
  this->altura = altura;
}
void Imagem::setLargura(float largura){
  this->largura = largura;
}
void Imagem::setComentario(string comentario){
  this->comentario = comentario;
}
void Imagem::setNumero_Magico(string numero_magico){
  this->numero_magico = numero_magico;
}
void Imagem::setNumero_Maximo_Cor(int numero_maximo_cor){
  this->numero_maximo_cor = numero_maximo_cor;
}
float Imagem::getAltura(){
  return altura;
}
float Imagem::getLargura(){
  return largura;
}
string Imagem::getTipoImagem(){
  return imagem;
}
string Imagem::getComentario(){
  return comentario;
}
