#include <iostream>
#include <cstdlib>
#include <cstdio>
#include "interface.hpp"

using namespace std;

Interface::Interface(){
  setNum_escolha(0);
}

void Interface::setNum_escolha(int num_escolha){
  this->num_escolha = num_escolha;
}
int Interface::getNum_escolha(){
  return num_escolha;
}

void Interface::Menu(){
  int numero_menu;

  cout << "(1) PPM" << endl;
  cout << "(2) PGM" << endl;
  cout << "Qual o tipo de arquivo que deseja abrir: ";
  cin >> numero_menu;

  if(numero_menu == 1){
    DecifraPPM * decifraPPM = new DecifraPPM();
    decifraPPM -> decifra_imagem_ppm();
  }

  else if (numero_menu == 2){
    DecifraPGM * decifraPGM = new DecifraPGM();
    decifraPGM->Segredo_Escondido_PGM();
  }

}
