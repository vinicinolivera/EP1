#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <string>

using namespace std;

class Imagem {
  //Atributos
protected:
    float altura; //std espaços de nomes
    float largura;
    int numero_maximo_cor;
    string comentario;
    string numero_magico;
    string imagem;

  public:
    //Contrutores
    Imagem();
    //Acessores
    void setTipoImagem (string imagem);
    string getTipoImagem();
    void setAltura (float altura);
    float getAltura();
    void setLargura(float largura);
    float getLargura();
    void setComentario(string comentario);
    string getComentario();
    void setNumero_Magico(string numero_magico);
    string getNumero_Magico();
    void setNumero_Maximo_Cor(int numero_maximo_cor);
    int getNumero_Magico_Cor();
    void AbreImagem();
      // void byte2bits(char c, string s);


};

#endif
