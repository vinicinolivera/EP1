#ifndef DECIFRAPPM_HPP
#define DECIFRAPPM_HPP
#include "decifra.hpp"


using namespace std;

class DecifraPPM : public Decifra{

  public:
    DecifraPPM();
    //DecifraPGM(string imagem);
    DecifraPPM(float largura, float altura);
    //DecifraPGM(float altura);
    void decifra_imagem_ppm();

};

#endif
