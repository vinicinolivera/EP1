#ifndef INTERFACE_HPP
#define INTERFACE_HPP
#include "imagem.hpp"
#include "decifraPGM.hpp"
#include "decifraPPM.hpp"
//#include "decifraPPM.cpp"

using namespace std;

class Interface{

  private:
    int num_escolha;

  public:
    Interface();
    void setNum_escolha(int num_escolha);
    int getNum_escolha();
    void Menu();
    //void Segredo_Escondido_PGM();
};

#endif
