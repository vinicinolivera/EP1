#ifndef DECIFRA_HPP
#define DECIFRA_HPP
#include <string>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <iomanip>
#include <cstring>
#include <cstdlib>
#include <bitset>
#include <istream>
#include <fstream>
//#include "imagem.hpp"

using namespace std;

class Decifra {
  //Atributos
private:
    float altura; //std espaços de nomes
    float largura;
    string imagem;

  public:
    //Contrutores
    Decifra();
    //Acessores
    void setTipoImagem (string imagem);
    string getTipoImagem();
    void setAltura (float altura);
    float getAltura();
    void setLargura(float largura);
    float getLargura();

};

#endif
