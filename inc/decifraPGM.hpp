#ifndef DECIFRAPGM_HPP
#define DECIFRAPGM_HPP
#include "imagem.hpp"
#include "decifra.hpp"

using namespace std;

class DecifraPGM : public Decifra{

  public:
    DecifraPGM();
    // DecifraPGM(string imagem);
    // DecifraPGM(float altura);
    void Segredo_Escondido_PGM();
    //void AbreImagem();

};

#endif
